import XCTest
@testable import mancala_lobby_core

class mancala_lobby_coreTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(mancala_lobby_core().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
