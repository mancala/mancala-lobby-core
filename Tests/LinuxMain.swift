import XCTest
@testable import mancala_lobby_coreTests

XCTMain([
    testCase(mancala_lobby_coreTests.allTests),
])
