//
//  ManacalaLobbyMessages.swift
//
//  Created by Michael Sanford on 12/22/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore
import MancalaCore
import SwiftOnSockets

// MARK: Client --> Server

public struct PlayerNewGameRequest: ByteCoding {
    public static let type = ClientToLobbyMessageType.createGameRequest
    
    public let identifier: UUID
    public let playerInfo: MancalaPlayerInfo
    
    public init(identifier: UUID = UUID(), playerInfo: MancalaPlayerInfo) {
        self.identifier = identifier
        self.playerInfo = playerInfo
    }
    
    public init?(message: Message) {
        guard message.type == PlayerNewGameRequest.type.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerNewGameRequest due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.identifier = try unarchiver.decodeUUID()
        } catch {
            assert(false, "failed to unarchive PlayerNewGameRequest due to bad format")
            return nil
        }
        guard let playerInfo = MancalaPlayerInfo(unarchiver: unarchiver) else { return nil }
        self.playerInfo = playerInfo
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(identifier)
        archiver.encode(playerInfo)
    }
}

// MARK: Server --> Client

public struct MancalaGameInfo: ByteCoding {
    
    public let opponentInfo: MancalaPlayerInfo
    public let startingPlayer: PlayerIdentity
    
    public init(opponentInfo: MancalaPlayerInfo, startingPlayer: PlayerIdentity) {
        self.opponentInfo = opponentInfo
        self.startingPlayer = startingPlayer
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let opponentInfo = MancalaPlayerInfo(unarchiver: unarchiver),
            let startingPlayer = PlayerIdentity(unarchiver: unarchiver) else {
                assert(false, "failed to unarchive MancalaGameInfo due to bad format")
                return nil
        }
        
        self.opponentInfo = opponentInfo
        self.startingPlayer = startingPlayer
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(opponentInfo)
        archiver.encode(startingPlayer)
    }
}
