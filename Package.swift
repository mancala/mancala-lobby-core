// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "MancalaLobbyCore",
    products: [
        .library(
            name: "MancalaLobbyCore",
            targets: ["MancalaLobbyCore"]),
        ],
    dependencies: [
        .package(url: "git@gitlab.com:msanford1030/SoS.git", from: "1.2.0"),
        .package(url: "git@gitlab.com:msanford1030/NetworkingCore.git", from: "1.2.0"),
        .package(url: "git@gitlab.com:mancala/gaming-core.git", from: "1.0.1"),
        .package(url: "git@gitlab.com:mancala/mancala-core.git", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "MancalaLobbyCore",
            dependencies: ["SwiftOnSockets", "NetworkingCore", "MancalaCore", "GamingCore"]),
        ]
)
